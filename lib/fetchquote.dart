import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

// fetch future daily quote
Future<DailyQ> getDailyQ() async {
  print('Calling daily q');
  final response = await http.get('https://eddyizm.com/quotes/daily/');

  if (response.statusCode == 200) {
    // If the call to the server was successful, parse the JSON
    var quoteResponse = json.decode(response.body);
    var newQuote = new DailyQ();
    newQuote.quote = quoteResponse['quote'];
    return newQuote;
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load post');
  }
}

// fetch future random quote
Future<RandomQ> getRandomQ() async {
  print('Calling random q');
  final response = await http.get('https://eddyizm.com/quotes/random/');

  if (response.statusCode == 200) {
    // If the call to the server was successful, parse the JSON
    var quoteResponse = json.decode(response.body);
    var newQuote = new RandomQ();
    newQuote.quote = quoteResponse['quote'];
    print(newQuote.quote);
    return newQuote;
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load post');
  }
}

// quote objects
class DailyQ {
  String quote;
  String category;
  String dateSent;
}

class RandomQ {
  String quote;
  String id;
}
