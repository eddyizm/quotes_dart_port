import 'package:flutter/material.dart';
import 'fetchquote.dart';

void main() => runApp(MaterialApp(
      title: 'Daily Quotes',
      theme: ThemeData(
        primaryColor: Colors.blue[300],
        primaryColorDark: Colors.blue[900],
        scaffoldBackgroundColor: Colors.blue[100],
      ),
      home: MyApp(
        quote: getDailyQ(),
      ),
    ));

// class MyApp extends StatelessWidget {
//   final Future<DailyQ> quote;
//   final qStyle = new TextStyle(color: Colors.blueGrey, fontSize: 25);

//   MyApp({Key key, this.quote}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Daily Quotes',
//       theme: ThemeData(
//         primaryColor: Colors.blue[300],
//         primaryColorDark: Colors.blue[900],
//         scaffoldBackgroundColor: Colors.blue[100],
//       ),
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text('Daily Quotes'),
//         ),
//         body: new Container(
//         padding: new EdgeInsets.all(20.0),
//         child:
//         Center(
//           child: FutureBuilder<DailyQ>(
//             future: quote,
//             builder: (context, snapshot) {
//               if (snapshot.hasData) {
//                 return Text(snapshot.data.quote, style: qStyle);
//               } else if (snapshot.hasError) {
//                 print(snapshot.toString());
//                 return Text("${snapshot.error}");
//               }
//               // By default, show a loading spinner
//               return CircularProgressIndicator();
//             },
//           ),
//         ),
//         ),
//         floatingActionButton: FloatingActionButton.extended(
//             elevation: 4.0,
//             icon: const Icon(Icons.chat_bubble_outline),
//             label: const Text('Random Quote'),
//             //child: Icon(Icons.chat_bubble),
//             onPressed: () {
//               print('floating action button pressed');
//             }),
//         floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
//         bottomNavigationBar: BottomAppBar(
//           child: new Row(
//             mainAxisSize: MainAxisSize.max,
//             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             children: <Widget>[
//               IconButton(
//                 icon: Icon(Icons.search),
//                 onPressed: () {
//                   print('search click');
//                 },
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }

class MyApp extends StatelessWidget {
  final Future<DailyQ> quote;
  final qStyle = new TextStyle(color: Colors.blueGrey, fontSize: 25);

  MyApp({Key key, this.quote}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Daily Quotes'),
      ),
      body: new Container(
        padding: new EdgeInsets.all(20.0),
        child: Center(
          child: FutureBuilder<DailyQ>(
            future: quote,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return Text(snapshot.data.quote, style: qStyle);
              } else if (snapshot.hasError) {
                print(snapshot.toString());
                return Text("${snapshot.error}");
              }
              // By default, show a loading spinner
              return CircularProgressIndicator();
            },
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
          elevation: 4.0,
          icon: const Icon(Icons.chat_bubble_outline),
          label: const Text('Random Quote'),
          //child: Icon(Icons.chat_bubble),
          onPressed: () {
            print('floating action button pressed');
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => RandomQuoteRoute(
                        randomquote: getRandomQ(),
                      )),
            );
          }),
      floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
      bottomNavigationBar: BottomAppBar(
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.search),
              onPressed: () {
                print('search click');
              },
            ),
          ],
        ),
      ),
    );
  }
}

// stateful widget test
class RandomQuoteRoute extends StatelessWidget {
  RandomQuoteRoute({Key key, this.randomquote}) : super(key: key);
  final Future<RandomQ> randomquote;
  final qStyle = new TextStyle(color: Colors.blueGrey, fontSize: 25);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Random Quote"),
      ),
      body: Container(
        padding: new EdgeInsets.all(20.0),
        child: Center(
          child: FutureBuilder<RandomQ>(
            future: randomquote,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return Text(snapshot.data.quote, style: qStyle,);
              } else if (snapshot.hasError) {
                print(snapshot.toString());
                return Text("${snapshot.error}");
              }
              return CircularProgressIndicator();
            },
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
          elevation: 4.0,
          icon: const Icon(Icons.chat_bubble_outline),
          label: const Text('Random Quote'),
          onPressed: () {
            print('floating action on random q page');
            Navigator.pop(context);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => RandomQuoteRoute(
                randomquote: getRandomQ(),
              )),
            );
          }),
      floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
      bottomNavigationBar: BottomAppBar(
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.search),
              onPressed: () {
                print('search click');
              },
            ),
          ],
        ),
      ),
    );
  }
}
